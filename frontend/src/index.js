import "bootstrap/dist/css/bootstrap.min.css";
import "./css/style.css";

import loginPage from "./html/login.html";
import homePage from "./html/home.html";
import { getTokenFromStorage } from "./js/util.js";
import { addLogoutHandler, getLoggedInUser } from "./js/home.js";
import { addLoginHandler } from "./js/login.js";

// Haal het element op waarin we nieuwe HTML zullen tonen.
const pageContent = document.getElementById("content");

// Functie die de juiste HTML laadt en handler toevoegt in functie van de aanwezigheid van een token.
function loadPageContent() {
  getTokenFromStorage().then((token) => {
    if (!token) {
      // Unauthenticated.
      pageContent.innerHTML = loginPage;
      addLoginHandler();
    } else {
      // Authenticated
      pageContent.innerHTML = homePage;
      addLogoutHandler();
      getLoggedInUser().then((data) => {
        document.getElementById("user-data").innerHTML = data.address;
      });
    }
  });
}

// Roept gewoon bovenstaande functie aan, maar wordt geëxporteerd onder een andere naam
// zodat deze functie uit andere modules aangeroepen kan worden en uit de naam blijkt wat er zal gebeuren.
export const refreshContent = () => loadPageContent();

// Voer de functie een eerste maal uit om content op het scherm te tonen.
loadPageContent();
