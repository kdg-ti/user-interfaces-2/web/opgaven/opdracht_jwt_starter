export const LOGIN_ROUTE = "/login";
export const USERS = "/users";
export const CURRENT_USER = `${USERS}/me`;
