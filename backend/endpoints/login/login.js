// Deze file volgt het 'action pattern':https://ponyfoo.com/articles/action-pattern-clean-obvious-testable-code
import { createJWT } from "../../auth";
import { findUserByUsernameWithPassword } from "../../database/simulatedDB";
import { compare } from "../../database/util";
import {
  MSG_NO_USER_FOUND,
  MSG_PASSWORD_UNDEFINED,
  MSG_USERNAME_UNDEFINED,
  MSG_WRONG_PASSWORD,
} from "../../util/errorHander";

function validate({ username, password }) {
  try {
    if (!(typeof username === "string"))
      throw new Error(MSG_USERNAME_UNDEFINED);
    if (!(typeof password === "string"))
      throw new Error(MSG_PASSWORD_UNDEFINED);
  } catch (error) {
    console.error(`(validate): ${error}`);
    throw error;
  }
}

async function getUser(username) {
  try {
    const userFromDB = await findUserByUsernameWithPassword(username);
    if (!userFromDB) throw new Error(MSG_NO_USER_FOUND);
    return userFromDB;
  } catch (error) {
    console.error(`(getUser):${error}`);
    throw error;
  }
}

async function checkPassword(user, password) {
  try {
    const passwordsMatch = await compare(password, user.hashed_password);
    if (!passwordsMatch) throw new Error(MSG_WRONG_PASSWORD);
  } catch (error) {
    console.error(`(checkPassword): ${error}`);
    throw error;
  }
}

export default async function login(req, res, next) {
  try {
    validate(req.body);
    const { username, password } = req.body;
    const user = await getUser(username);

    await checkPassword(user, password);

    const token = await createJWT(user);
    return res.send({ token });
  } catch (error) {
    //Geef de error door aan de error handler.
    next(new Error(`[Login] ${error}`));
  }
}
