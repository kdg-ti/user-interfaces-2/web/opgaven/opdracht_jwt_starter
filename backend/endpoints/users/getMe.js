import { extractTokenFromRequest, verifyJWT } from "../../auth";
import { findUserById } from "../../database/simulatedDB";

export default async function getMe(req, res, next) {
  try {
    const plainToken = await extractTokenFromRequest(req);
    const decodedInfo = await verifyJWT(plainToken);
    console.log("decodedInfo:", decodedInfo);
    // Door de informatie uit de token te halen, zijn we er zeker van dat
    // deze user enkel de data te zien krijgt voor de user waarvoor deze token
    // geldig is. We zouden in de token bv ook rol informatie kunnen zetten (role: 'ADMIN')
    // die aangeeft welke rechten een bepaalde user heeft.
    const me = await findUserById(decodedInfo.userId);

    res.send(me);
  } catch (error) {
    console.error(`[getMe]: ${error}`);
    next(error);
  }
}
