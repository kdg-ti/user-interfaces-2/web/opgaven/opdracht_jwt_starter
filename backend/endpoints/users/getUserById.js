import { findUserById } from "../../database/simulatedDB";

export default function getUserById(id) {
  // TODO: validate token
  console.log(`getUserById: searching for user with id: ${id}`);
  return findUserById(id);
}
