import { insertNewUser } from "./simulatedDB";

const testUsers = [
  {
    username: "test@testapp.be",
    password: "Veilig@456",
    firstName: "Test",
    lastName: "User",
    address: "Teststraat 123, 1234 Teststad",
    phone: "0123/456789",
  },
  {
    username: "newuser@testapp.be",
    password: "PWD@789",
    firstName: "Test 1",
    lastName: "User 1",
    address: "Teststraat 124, 1234 Teststad",
    phone: "0123/456780",
  },
  {
    username: "newuser2@testapp.be",
    password: "PWD@123",
    firstName: "Test 2",
    lastName: "User 2",
    address: "Teststraat 125, 1234 Teststad",
    phone: "0123/456781",
  },
];

export default function seed() {
  testUsers.forEach(async (userToInsert) => await insertNewUser(userToInsert));
}
